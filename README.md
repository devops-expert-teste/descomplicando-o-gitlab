# Descomplicando o Gitlab

Treinamento Descomplicando o Gitlab.


### Day-1

- Entendemos o que é o Git
- Entendemos o que é o Working Dir, Index e HEAD
- Entendemos o que é o Gitlab
- Como criar um Grupo no Gitlab
- Aprendemos os comandos básicos para manipulação de arquivos e diretórios no git
- Como criar uma branch
- Como criar um Merge Request
- Como criar um Merge Request
- Como adicionar um Membro no projeto
- Como fazer o merge na Master/Main
- Como associar um repo local com um repo remoto
- Como importar um repo do GitHub para o Gitlab